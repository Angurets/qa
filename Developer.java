package Task2;

public class Developer extends Employer {


    @Override
    public void earnSalary(int money) {
        System.out.println("The developer's salary is $:" + money);

    }

    @Override
    public void Project(String test) {
        System.out.println("The developer works on:" + test);

    }
}
