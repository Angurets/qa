package Task2;

public abstract class Employer implements Person {

    public abstract void earnSalary(int money);
}
